import styled from '@emotion/styled';
import { Box, themeProps } from 'components/design-system';

interface NavGridProps {
  noPadding?: boolean;
}

export const NavGrid = styled(Box)<NavGridProps>`
  display: grid;
  grid-template-columns: 1fr 1fr minmax(auto, ${themeProps.widths.md}px) 1fr 1fr;
  padding: ${props => (props.noPadding ? 0 : '0 24px')};
  z-index: 50;

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: 1fr 1fr minmax(auto, ${themeProps.widths.lg}px) 1fr 1fr;
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: 1fr 1fr minmax(auto, ${themeProps.widths.xl}px) 1fr 1fr;
  }
`;

export const NavInner = styled(Box)`
  grid-column: 3/4;
`;
