import styled from '@emotion/styled';
import {
  color,
  ColorProps,
  layout,
  LayoutProps,
  space,
  SpaceProps,
  typography,
  TypographyProps as SystemTypographyProps,
  variant,
} from 'styled-system';
import shouldForwardProp from '@styled-system/should-forward-prop';
import { ParagraphScale } from '../../Theme';

export interface VerseProps extends LayoutProps, SpaceProps, ColorProps, SystemTypographyProps {
  as?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
  color?: string;
  variant?: ParagraphScale;
}

const Verse = styled('p', { shouldForwardProp })<VerseProps>(
  variant({ scale: 'componentStyles.typography.textScale' }),
  layout,
  space,
  color,
  typography
);

Verse.defaultProps = {
  variant: 400,
};

Verse.displayName = 'Verse';

export default Verse;
